from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    #Define the params for the request
    params = {
        "query": city + " " + state
    }
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search?"
    # Make the request
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    content = response.content
    parsed_json = json.loads(content)
    picture = parsed_json["photos"][0]["src"]["original"]
    
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return {
        "picture_url": picture
    }
    
def get_lat_long(location):
    """_
    Returns the latitude and longitude for the specified location
    using the OpenWeatherMap API.
    
    """
    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY
    }
    
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    return{
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"]
    }
    
    
def get_weather_data(location): 
     """
     Returns the main temperature and the weather's description for the
     specified location using the OpenWeatherMap API.
     
     """
     lat_long = get_lat_long(location)
     base_url = "http://api.openweathermap.org/data/2.5/weather"
     params = {
         "lat": lat_long["latitude"],
         "lon": lat_long["longitude"],
         "appid": OPEN_WEATHER_API_KEY,
         "units": "imperial"
     }
    #     Create the URL for the geocoding API with the city and state
    # Make the request
    
     response = requests.get(base_url, params=params)
    
    # Parse the JSON response
     parsed_json = json.loads(response.content)
    # Get the latitude and longitude from the response
     weather_data = {
        "temperature": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"]
    }
    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
     return weather_data